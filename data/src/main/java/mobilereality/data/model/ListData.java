package mobilereality.data.model;

import java.util.Date;

import mobilereality.data.util.DataConstants;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class ListData extends BaseData implements Comparable<ListData> {
    Date created_at;
    int state;

    public ListData(){}

    public ListData(String name){
        this.name = name;
        this.created_at = new Date();
        this.state = DataConstants.ACTUAL_LISTS_STATE;
    }

    @Override
    public Long getId() {
        return super.getId();
    }


    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }


    public Date getCreatedAt() {
        return created_at;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    @Override
    public int compareTo(ListData another) {
        return getCreatedAt().compareTo(another.getCreatedAt());
    }
}
