package mobilereality.data.model;

import com.orm.SugarRecord;

/**
 * Created by Mateusz on 2016-10-05.
 */
public class BaseData extends SugarRecord {

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
