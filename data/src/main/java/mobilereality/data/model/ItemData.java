package mobilereality.data.model;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class ItemData extends BaseData {
    ListData listData;

    public ItemData(){}

    public ItemData(String name, ListData listData){
        this.name = name;
        this.listData = listData;
    }

    @Override
    public Long getId() {
        return super.getId();
    }

    public ListData getListData() {
        return listData;
    }

    public void setListData(ListData listData) {
        this.listData = listData;
    }

}
