package mobilereality.data.sharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

/**
 * Created by Mateusz on 2015-08-10.
 */
public class SharedPreferencesManager {

    private Context ctx;

    public static final String APPLICATION_PREFERENCES = "appPreferences";


    @Inject
    public SharedPreferencesManager(Context ctx){
        this.ctx = ctx;
    }

    public void setDataBoolean(String data, boolean value) {

        SharedPreferences prefData = ctx.getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefData.edit();
        editor.putBoolean(data, value);
        editor.apply();

    }

    public  boolean getDataBoolean( String data) {

        SharedPreferences prefData = ctx.getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        return prefData.getBoolean(data, false);

    }

    public  int getDataInt(String data) {

        SharedPreferences prefData = ctx.getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        return prefData.getInt(data, 0);

    }

    public  void setDataInt(String data, int value) {

        SharedPreferences prefData = ctx.getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefData.edit();
        editor.putInt(data, value);
        editor.apply();

    }

    public String getDataString(String data) {

        SharedPreferences prefData = ctx.getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        return prefData.getString(data, null);

    }

    public  void setDataString(String data, String value) {

        SharedPreferences prefData = ctx.getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefData.edit();
        editor.putString(data, value);
        editor.apply();

    }

    public  void clearData() {

        SharedPreferences prefData = ctx.getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefData.edit();
        editor.clear();
        editor.apply();

    }

    public boolean checkIfContains(String value) {
        SharedPreferences prefData = ctx.getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        return prefData.contains(value);
    }

    public  void deleteData(String data) {

        SharedPreferences prefData = ctx.getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefData.edit();
        editor.remove(data);
        editor.apply();
    }

}

