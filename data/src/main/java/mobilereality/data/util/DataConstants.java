package mobilereality.data.util;

/**
 * Created by Mateusz on 2016-10-05.
 */
public class DataConstants {

    public static final String LISTS_STATE_TAG = "listsState";
    public static final String ACTIVE_VIEW_TAG = "activeView";

    public static final int ARCHIVED_LISTS_STATE = -1;
    public static final int ACTUAL_LISTS_STATE = 1;

}
