package mobilereality.data.query;

import java.util.ArrayList;
import java.util.List;

import mobilereality.data.model.ListData;
import mobilereality.data.util.DataConstants;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class ListQuery {

    public static ArrayList<ListData> getActualLists(){
        ArrayList<ListData> listsList = new ArrayList<>();
        List<ListData> list = ListData.find(
                ListData.class,
                "STATE = ?",
                "" + DataConstants.ACTUAL_LISTS_STATE
        );
        for(ListData listData : list){
            listsList.add(listData);
        }
        return listsList;
    }

    public static ArrayList<ListData> getArchivedLists(){
        ArrayList<ListData> listsList = new ArrayList<>();
        List<ListData> list = ListData.find(
                ListData.class,
                "STATE = ?",
                ""+ DataConstants.ARCHIVED_LISTS_STATE
        );
        for(ListData listData : list){
            listsList.add(listData);
        }
        return listsList;
    }

    public static boolean addList(String name){
        ListData list = new ListData(name);
        list.save() ;
        return true;
    }

    public static boolean updateListState(ListData list, int listState){
        list.setState(listState);
        list.save();
        return true;
    }

}
