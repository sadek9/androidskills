package mobilereality.data.query;

import java.util.ArrayList;
import java.util.List;

import mobilereality.data.model.ItemData;
import mobilereality.data.model.ListData;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class ItemQuery {

    public static boolean addItemToList(String name, ListData listData){
        ItemData itemData = new ItemData(name,listData);
        itemData.save();
        return true;
    }

    public static ArrayList getItemsFromList(ListData listData){
        ArrayList<ItemData> itemsList = new ArrayList<>();
        List<ItemData> list = ListData.find(
                ItemData.class,
                "LIST_DATA = ?",
                ""+listData.getId()
        );
        for(ItemData itemData : list){
            itemsList.add(itemData);
        }
        return itemsList;
    }

}
