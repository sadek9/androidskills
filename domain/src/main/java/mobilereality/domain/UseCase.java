package mobilereality.domain;

import java.util.ArrayList;
import java.util.Collections;

import mobilereality.data.model.ListData;
import mobilereality.data.query.ItemQuery;
import mobilereality.data.query.ListQuery;
import mobilereality.data.util.DataConstants;

/**
 * Created by Mateusz on 2016-10-05.
 */
public class UseCase {

    public static ArrayList getSortedLists(int state){
        ArrayList list;
        if(state== DataConstants.ARCHIVED_LISTS_STATE) {
            list = ListQuery.getArchivedLists();
            Collections.sort(list);
        } else {
            list = ListQuery.getActualLists();
            Collections.sort(list);
        }
        return list;

    }

    public static boolean addList(String name){
        return ListQuery.addList(name);
    }

    public static boolean addItem(String name, ListData list){
        return ItemQuery.addItemToList(name, list);
    }

    public static boolean updateListState(ListData list, int listState){
        return ListQuery.updateListState(list, listState);
    }

    public static ArrayList getItemsFromList(ListData list){
        return ItemQuery.getItemsFromList(list);
    }

}
