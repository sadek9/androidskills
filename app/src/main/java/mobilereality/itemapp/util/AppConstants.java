package mobilereality.itemapp.util;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class AppConstants {
    public static final int LISTS_FRAGMENT_VIEW_ID = 0;
    public static final int SINGLE_LIST_FRAGMENT_VIEW_ID = 1;

    public static final int LIST_ROW_TYPE = 0;
    public static final int ITEM_ROW_TYPE = 1;
}
