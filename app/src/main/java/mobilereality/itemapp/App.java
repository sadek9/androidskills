package mobilereality.itemapp;

import android.app.Application;

import com.orm.SugarContext;

import net.danlew.android.joda.JodaTimeAndroid;

import mobilereality.itemapp.injector.component.AppComponent;
import mobilereality.itemapp.injector.component.DaggerAppComponent;
import mobilereality.itemapp.injector.module.AppModule;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class App extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
        JodaTimeAndroid.init(this);
        setupInjector();
    }

    private void setupInjector() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }


    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }


}


