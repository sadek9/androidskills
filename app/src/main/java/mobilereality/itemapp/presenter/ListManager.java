package mobilereality.itemapp.presenter;

import java.util.ArrayList;

import javax.inject.Inject;

import mobilereality.data.model.ListData;
import mobilereality.data.sharedPreferences.SharedPreferencesManager;
import mobilereality.data.util.DataConstants;
import mobilereality.domain.UseCase;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class ListManager {

    SharedPreferencesManager sharedPreferencesManager;
    ListCoordinator coordinator;
    ListData chosenList;

    @Inject
    public ListManager (SharedPreferencesManager sharedPreferencesManager){
        this.sharedPreferencesManager = sharedPreferencesManager;
    }

    public void setCoordinator(ListCoordinator coordinator){
        this.coordinator = coordinator;
    }

    public ListCoordinator getCoordinator() {
        return this.coordinator;
    }

    public ArrayList getLists(){
       return UseCase.getSortedLists(getListsState());
    }

    public int getListsState(){
        return sharedPreferencesManager.getDataInt(DataConstants.LISTS_STATE_TAG);
    }

    public void setListsState(int state){
        sharedPreferencesManager.setDataInt(DataConstants.LISTS_STATE_TAG,state);
    }

    public boolean addList(String name){
        return UseCase.addList(name);
    }

    public interface ListCoordinator{
        void onChange();
    }

    public void setChosenList(ListData chosenList) {
        this.chosenList = chosenList;
    }

    public ListData getChosenList() {
        return chosenList;
    }

    public boolean addItem(String name){
        return UseCase.addItem(name,getChosenList());
    }

    public ArrayList getItemFromList(){
        return UseCase.getItemsFromList(getChosenList());
    }
}
