package mobilereality.itemapp.presenter;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import javax.inject.Inject;

import mobilereality.itemapp.util.FontCache;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class StyleManager {

    Context context;

    @Inject
    public StyleManager(Context ctx) {
        this.context = ctx;
    }

    public void setCustomFont(TextView textView, String font) {
        if (font == null) {
            return;
        }
        Typeface tf = FontCache.get(font, context);
        if (tf != null) {
            textView.setTypeface(tf);
        }
    }

    public void setRobotoLightFont(TextView textView) {
        Typeface tf = FontCache.get("Roboto-Light.ttf", context);
        if (tf != null) {
            textView.setTypeface(tf);
        }
    }

    public void setRobotoBoldtFont(TextView textView) {
        Typeface tf = FontCache.get("Roboto-Bold.ttf", context);
        if (tf != null) {
            textView.setTypeface(tf);
        }
    }

}
