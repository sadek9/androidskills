package mobilereality.itemapp.injector.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import mobilereality.data.sharedPreferences.SharedPreferencesManager;
import mobilereality.itemapp.injector.scope.PerApp;

/**
 * Created by Mateusz on 2016-10-05.
 */
@Module
public class StorageModule {

    @Provides
    @PerApp
    public SharedPreferencesManager provideSharedPreferencesManager(Context ctx) {
        return new SharedPreferencesManager(ctx);
    }
}
