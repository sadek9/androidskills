package mobilereality.itemapp.injector.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import mobilereality.itemapp.App;
import mobilereality.itemapp.injector.scope.PerApp;

/**
 * Created by Mateusz on 2016-10-04.
 */
@Module
public class AppModule {

    private final App app;
    private final Context context;

    public AppModule(App app) {
        this.app = app;
        this.context = app.getBaseContext();
    }

    @Provides
    @PerApp
    public App provideApp() {
        return app;
    }

    @Provides
    @PerApp
    public Context provideContext() {
        return app.getApplicationContext();
    }

}
