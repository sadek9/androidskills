package mobilereality.itemapp.injector.component;

import android.content.Context;

import dagger.Component;
import mobilereality.data.sharedPreferences.SharedPreferencesManager;
import mobilereality.itemapp.App;
import mobilereality.itemapp.injector.module.AppModule;
import mobilereality.itemapp.injector.module.StorageModule;
import mobilereality.itemapp.injector.scope.PerApp;

/**
 * Created by Mateusz on 2016-10-04.
 */
@PerApp
@Component(
        modules = {
                AppModule.class,
                StorageModule.class
        }
)
public interface AppComponent {
    App app();
    Context context();
    SharedPreferencesManager sharedPreferencesManager();
}
