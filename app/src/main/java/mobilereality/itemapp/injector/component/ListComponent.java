package mobilereality.itemapp.injector.component;

/**
 * Created by Mateusz on 2016-10-05.
 */

import dagger.Component;
import mobilereality.itemapp.injector.scope.PerActivity;
import mobilereality.itemapp.view.activity.MainActivity;

@PerActivity
@Component(dependencies = AppComponent.class)
public interface ListComponent {
    void inject(MainActivity mainActivity);
}
