package mobilereality.itemapp.injector.scope;

import javax.inject.Scope;

/**
 * Created by Mateusz on 2016-10-04.
 */
@Scope
public @interface PerApp {
}
