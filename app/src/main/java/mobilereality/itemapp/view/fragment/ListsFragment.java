package mobilereality.itemapp.view.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.Bind;
import butterknife.ButterKnife;
import mobilereality.itemapp.R;
import mobilereality.itemapp.presenter.ListManager;
import mobilereality.itemapp.util.AppConstants;
import mobilereality.itemapp.view.adapter.ListAdapter;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class ListsFragment extends BaseFragment implements ListManager.ListCoordinator {

    @Bind(R.id.list)
    RecyclerView list;

    View listsFragmentView;
    private ListAdapter listAdapter;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        listsFragmentView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, listsFragmentView);
        listener.setActiveView(AppConstants.LISTS_FRAGMENT_VIEW_ID);
        initAdapter();
        return listsFragmentView;
    }

    private void initAdapter(){
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        list.setLayoutManager(linearLayoutManager);

        listAdapter = new ListAdapter(this.getActivity(),
                AppConstants.LIST_ROW_TYPE,
                listener.getListManager().getLists(),
                listener.getListManager().getListsState());
        list.setAdapter(listAdapter);
        listener.getListManager().setCoordinator(this);
    }


    @Override
    public void onChange() {
        listAdapter.setList(listener.getListManager().getLists());
        listAdapter.notifyDataSetChanged();
    }
}
