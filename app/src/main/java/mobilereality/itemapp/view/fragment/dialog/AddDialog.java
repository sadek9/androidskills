package mobilereality.itemapp.view.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import mobilereality.itemapp.R;
import mobilereality.itemapp.util.AppConstants;
import mobilereality.itemapp.view.fragment.BaseFragment;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class AddDialog extends BaseFragment {

    @Bind(R.id.dialogHeader)
    TextView dialogHeader;

    @Bind(R.id.dialogImage)
    ImageView dialogImage;

    @Bind(R.id.inputName)
    EditText inputName;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity().getBaseContext(), R.layout.dialog_add, null);
        ButterKnife.bind(AddDialog.this, view);
        switch (listener.getActiveView()){
            case AppConstants.LISTS_FRAGMENT_VIEW_ID:
                dialogImage.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.list));
                dialogHeader.setText(getActivity().getResources().getString(R.string.add_list_heder));
                inputName.setHint(getActivity().getResources().getString(R.string.add_list_hint));

                break;
            case AppConstants.SINGLE_LIST_FRAGMENT_VIEW_ID:
                dialogImage.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.item));
                dialogHeader.setText(getActivity().getResources().getString(R.string.add_item_header));
                inputName.setHint(getActivity().getResources().getString(R.string.add_item_hint));
                break;
        }
        listener.getStyleManager().setRobotoBoldtFont(dialogHeader);
        listener.getStyleManager().setRobotoLightFont(inputName);
        builder.setView(view)
                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        switch (listener.getActiveView()) {
                            case AppConstants.LISTS_FRAGMENT_VIEW_ID:
                                if(listener.getListManager().addList(inputName.getText().toString()))
                                    listener.getListManager().getCoordinator().onChange();
                                    AddDialog.this.getDialog().cancel();
                                break;
                            case AppConstants.SINGLE_LIST_FRAGMENT_VIEW_ID:
                                if(listener.getListManager().addItem(inputName.getText().toString()))
                                    listener.getListManager().getCoordinator().onChange();
                                AddDialog.this.getDialog().cancel();
                                break;
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AddDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
