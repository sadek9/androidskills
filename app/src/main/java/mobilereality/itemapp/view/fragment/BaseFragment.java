package mobilereality.itemapp.view.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;

import mobilereality.itemapp.presenter.ListManager;
import mobilereality.itemapp.presenter.StyleManager;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class BaseFragment extends DialogFragment {

    protected LinearLayoutManager linearLayoutManager;

    public CommunicationBridge listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (CommunicationBridge) activity;
    }

    public CommunicationBridge getListener() {
        return listener;
    }

    public interface CommunicationBridge {
        ListManager getListManager();
        void navigateTo(int fragmentID);
        StyleManager getStyleManager();
        void setActiveView(int viewId);
        int getActiveView();
    }

}
