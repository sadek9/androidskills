package mobilereality.itemapp.view.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import mobilereality.data.util.DataConstants;
import mobilereality.domain.UseCase;
import mobilereality.itemapp.R;
import mobilereality.itemapp.view.fragment.BaseFragment;

/**
 * Created by Mateusz on 2016-10-06.
 */
public class WarningDialog extends BaseFragment {
    @Bind(R.id.dialogHeader)
    TextView dialogHeader;

    @Bind(R.id.dialogImage)
    ImageView dialogImage;

    @Bind(R.id.inputName)
    TextView inputName;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity().getBaseContext(), R.layout.dialog_warning, null);
        ButterKnife.bind(WarningDialog.this, view);
        dialogImage.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.list));
        dialogHeader.setText(getActivity().getResources().getString(R.string.warning));

        listener.getStyleManager().setRobotoBoldtFont(dialogHeader);
        listener.getStyleManager().setRobotoLightFont(inputName);
        builder.setView(view)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (UseCase.updateListState(listener.getListManager().getChosenList(), DataConstants.ARCHIVED_LISTS_STATE)) {
                            listener.getListManager().getCoordinator().onChange();
                            WarningDialog.this.getDialog().cancel();
                        }
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        WarningDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
