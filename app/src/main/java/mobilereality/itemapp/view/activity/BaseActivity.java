package mobilereality.itemapp.view.activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import mobilereality.itemapp.util.AppConstants;
import mobilereality.itemapp.view.fragment.BaseFragment;
import mobilereality.itemapp.view.fragment.ListsFragment;
import mobilereality.itemapp.view.fragment.SingleListFragment;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class BaseActivity extends AppCompatActivity {

    public BaseFragment getNewFragment(int fragmentId) {
        if (fragmentId == AppConstants.LISTS_FRAGMENT_VIEW_ID)
            return new ListsFragment();
        if (fragmentId == AppConstants.SINGLE_LIST_FRAGMENT_VIEW_ID)
            return new SingleListFragment();
        else
            return null;
    }


    public void addFragment(int containerViewId, int fragmentID) {
        FragmentTransaction fragmentTransaction = this.getFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, getNewFragment(fragmentID));
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected void removeFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = this.getFragmentManager().beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commitAllowingStateLoss();
        }

    }

}
