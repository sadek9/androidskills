package mobilereality.itemapp.view.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mobilereality.data.sharedPreferences.SharedPreferencesManager;
import mobilereality.data.util.DataConstants;
import mobilereality.itemapp.App;
import mobilereality.itemapp.R;
import mobilereality.itemapp.injector.component.AppComponent;
import mobilereality.itemapp.injector.component.DaggerListComponent;
import mobilereality.itemapp.injector.component.ListComponent;
import mobilereality.itemapp.presenter.ListManager;
import mobilereality.itemapp.presenter.StyleManager;
import mobilereality.itemapp.util.AppConstants;
import mobilereality.itemapp.view.fragment.BaseFragment;
import mobilereality.itemapp.view.fragment.dialog.AddDialog;
import mobilereality.itemapp.view.fragment.dialog.WarningDialog;

public class MainActivity extends BaseActivity implements BaseFragment.CommunicationBridge {

    ListComponent listComponent;

    @Inject
    StyleManager styleManager;

    @Inject
    ListManager listManager;

    @Inject
    SharedPreferencesManager sharedPreferencesManager;

    @Bind(R.id.fab)
    FloatingActionButton fab;

    @Bind(R.id.toolbar)
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        inject();
        setSupportActionBar(toolbar);
        setInitialSettings();
        addFragment(R.id.fragment_container, AppConstants.LISTS_FRAGMENT_VIEW_ID);
    }

    private void inject(){
        AppComponent appComponent = ((App) getApplication()).getAppComponent();
        listComponent = DaggerListComponent.builder()
                .appComponent(appComponent)
                .build();
        listComponent.inject(this);
    }

    @OnClick(R.id.fab)
    public void onFabClick() {
        showAddDialog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.actual_lists) {
                goToActualList();
        } else if (id == R.id.archived_lists) {
                goToArchivedList();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void navigateTo(int fragmentID) {
        try {
            FragmentTransaction trans = this.getFragmentManager().beginTransaction();
            trans.replace(R.id.fragment_container, getNewFragment(fragmentID));
            trans.commitAllowingStateLoss();
        } catch (IllegalStateException ex) {
            Log.d(getResources().getString(R.string.TAG), "Actvity was destroyed");
        }
    }

    @Override
    public StyleManager getStyleManager() {
        return this.styleManager;
    }

    @Override
    public void setActiveView(int viewId) {
        sharedPreferencesManager.setDataInt(DataConstants.ACTIVE_VIEW_TAG, viewId);
    }

    @Override
    public int getActiveView() {
        return sharedPreferencesManager.getDataInt(DataConstants.ACTIVE_VIEW_TAG);
    }


    @Override
    public ListManager getListManager() {
        return this.listManager;
    }

    private void setInitialSettings(){
        listManager.setListsState(DataConstants.ACTUAL_LISTS_STATE);
    }

    public void showAddDialog() {
        AddDialog addDialog = new AddDialog();
        addDialog.show(getFragmentManager(), null);
    }

    public void showWarningDialog() {
        WarningDialog warningDialog= new WarningDialog();
        warningDialog.show(getFragmentManager(), null);
    }

    @Override
    public void onBackPressed() {
        if(getActiveView()==AppConstants.LISTS_FRAGMENT_VIEW_ID && listManager.getListsState() == DataConstants.ACTUAL_LISTS_STATE)
            finish();
        else if(getActiveView()==AppConstants.LISTS_FRAGMENT_VIEW_ID && listManager.getListsState() == DataConstants.ARCHIVED_LISTS_STATE)
            goToActualList();
        else if (getActiveView() == AppConstants.SINGLE_LIST_FRAGMENT_VIEW_ID && listManager.getListsState() == DataConstants.ARCHIVED_LISTS_STATE )
            goToArchivedList();
        else if(getActiveView() == AppConstants.SINGLE_LIST_FRAGMENT_VIEW_ID && listManager.getListsState() == DataConstants.ACTUAL_LISTS_STATE )
            goToActualList();
    }

    private void goToActualList(){
        fab.setVisibility(View.VISIBLE);
        setActiveView(AppConstants.LISTS_FRAGMENT_VIEW_ID);
        getListManager().setListsState(DataConstants.ACTUAL_LISTS_STATE);
        navigateTo(AppConstants.LISTS_FRAGMENT_VIEW_ID);
    }

    private void goToArchivedList(){
        fab.setVisibility(View.INVISIBLE);
        setActiveView(AppConstants.LISTS_FRAGMENT_VIEW_ID);
        getListManager().setListsState(DataConstants.ARCHIVED_LISTS_STATE);
        navigateTo(AppConstants.LISTS_FRAGMENT_VIEW_ID);
    }
}
