package mobilereality.itemapp.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import mobilereality.data.model.BaseData;
import mobilereality.data.model.ListData;
import mobilereality.itemapp.util.AppConstants;
import mobilereality.itemapp.view.activity.MainActivity;
import mobilereality.itemapp.view.customView.CustomHeaderList;
import mobilereality.itemapp.view.customView.CustomItemView;
import mobilereality.itemapp.view.customView.holder.CustomItemHolder;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class ListAdapter extends RecyclerView.Adapter<CustomItemHolder> {

    private int listType;
    private int listState;
    private Context context;
    private ArrayList<BaseData> list = new ArrayList<>();

    public ListAdapter(Context context, int listType, ArrayList list, int listState) {
        this.listType = listType;
        this.context = context;
        this.list = list;
        this.listState = listState;
    }

    public void setList(ArrayList list){
        this.list = list;
    }

    @Override
    public CustomItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 0)
            return new CustomItemHolder(new CustomHeaderList(context, listState),viewType);
        else
            return new CustomItemHolder(new CustomItemView(context, listType),viewType);
    }

    private View.OnClickListener getOnClickListener(final ListData list) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) context).getListManager().setChosenList(list);
                ((MainActivity) context).navigateTo(AppConstants.SINGLE_LIST_FRAGMENT_VIEW_ID);
            }
        };
    }

    private View.OnLongClickListener getOnLongClickListener(final ListData list) {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ((MainActivity) context).getListManager().setChosenList(list);
                ((MainActivity) context).showWarningDialog();
                return true;
            }
        };
    }

    @Override
    public void onBindViewHolder(CustomItemHolder holder, int position) {
        if(position != 0) {
            switch (listType) {
                case AppConstants.ITEM_ROW_TYPE:
                    holder.customItemView.setData(
                            listType,
                            position,
                            list.get(position-1).getName(),
                            null,
                            null,
                            null
                    );
                    break;
                case AppConstants.LIST_ROW_TYPE:
                    holder.customItemView.setData(
                            listType,
                            position,
                            list.get(position-1).getName(),
                            ((ListData) list.get(position-1)).getCreatedAt(),
                            getOnClickListener((ListData) list.get(position-1)),
                            getOnLongClickListener((ListData) list.get(position-1))
                    );
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
