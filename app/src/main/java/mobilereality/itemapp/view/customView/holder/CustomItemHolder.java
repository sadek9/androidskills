package mobilereality.itemapp.view.customView.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import mobilereality.itemapp.view.customView.CustomHeaderList;
import mobilereality.itemapp.view.customView.CustomItemView;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class CustomItemHolder extends RecyclerView.ViewHolder {

    public CustomItemView customItemView;
    public CustomHeaderList customHeaderList;

    public CustomItemHolder(View itemView, int viewType) {
        super(itemView);
        if(viewType == 0)
            customHeaderList = (CustomHeaderList)itemView;
        else
            customItemView = (CustomItemView)itemView;

    }
}
