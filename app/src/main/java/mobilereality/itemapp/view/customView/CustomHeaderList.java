package mobilereality.itemapp.view.customView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import mobilereality.data.util.DataConstants;
import mobilereality.itemapp.R;
import mobilereality.itemapp.view.activity.MainActivity;

/**
 * Created by Mateusz on 2016-10-06.
 */
public class CustomHeaderList extends RelativeLayout {

    @Bind(R.id.headerText)
    TextView header;

    public CustomHeaderList(Context context, int listType) {
        super(context);
        init(context,listType);
    }

    private void init(Context ctx, int listType){
        View view = LayoutInflater.from(ctx).inflate( R.layout.custom_list_header, this, true);
        ButterKnife.bind(this, view);
        ((MainActivity)ctx).getStyleManager().setRobotoBoldtFont(header);
        switch (listType){
            case DataConstants.ACTUAL_LISTS_STATE:
                header.setText(ctx.getResources().getText(R.string.actual));
                break;
            case DataConstants.ARCHIVED_LISTS_STATE:
                header.setText(ctx.getResources().getText(R.string.archived));
                break;

        }

    }
}
