package mobilereality.itemapp.view.customView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import mobilereality.itemapp.R;
import mobilereality.itemapp.util.AppConstants;

/**
 * Created by Mateusz on 2016-10-04.
 */
public class CustomItemView extends RelativeLayout {

    Context ctx;

    @Bind(R.id.icon)
    ImageView icon;

    @Bind(R.id.header)
    TextView header;

    @Bind(R.id.name)
    TextView name;

    @Bind(R.id.createAt)
    TextView createAt;

    @Bind(R.id.onClickArea)
    FrameLayout onClickArea;


    public CustomItemView(Context context, int itemType) {
        super(context);
        this.ctx = context;
        init(context,itemType);
    }

    public void init(Context context, int itemType){
        View view = LayoutInflater.from(ctx).inflate( R.layout.custom_item_view, this, true);
        ButterKnife.bind(this,view);
        switch (itemType){
            case AppConstants.LIST_ROW_TYPE:
                icon.setImageDrawable(context.getResources().getDrawable(R.drawable.list));
                break;
            case AppConstants.ITEM_ROW_TYPE:
                icon.setImageDrawable(context.getResources().getDrawable(R.drawable.item));
                createAt.setVisibility(INVISIBLE);
                break;

        }
    }

    public void setData(int itemType,
                        int number,
                        String nameContent,
                        Date date,
                        OnClickListener onClickListener,
                        OnLongClickListener onLongClickListener){
        switch (itemType){
            case AppConstants.LIST_ROW_TYPE:
                String listHeaderContent = ctx.getResources().getString(R.string.list_header) + number;
                header.setText(listHeaderContent);
                name.setText(nameContent);
                try {
                    createAt.setText(setDate(date));
                } catch (NullPointerException exception) {
                    createAt.setVisibility(INVISIBLE);
                }
                onClickArea.setOnClickListener(onClickListener);
                onClickArea.setOnLongClickListener(onLongClickListener);
                break;
            case AppConstants.ITEM_ROW_TYPE:
                String itemHeaderContent = ctx.getResources().getString(R.string.item_header) + number;
                header.setText(itemHeaderContent);
                name.setText(nameContent);
                break;

        }
    }

    private String setDate(Date date){
        DateTime parseDate = new DateTime(date);
        return  parseDate.getYear() + "/" +
                parseDate.getMonthOfYear() + "/" +
                parseDate.getDayOfMonth() + "  " +
                parseDate.getHourOfDay() + ":" +
                parseDate.getMinuteOfHour();
    }


}
